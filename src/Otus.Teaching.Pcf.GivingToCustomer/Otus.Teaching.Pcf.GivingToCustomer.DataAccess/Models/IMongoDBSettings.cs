﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Models
{
    public interface IMongoDBSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
